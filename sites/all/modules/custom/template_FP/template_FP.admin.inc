<?php

/**
 * @file
 * Module config form.
 */

/**
 * Admin form for module.
 */

function template_FP_admin_settings($form, &$form_state) {

    $form = array();
    $form['configuration'] = array(
        '#title' => 'Site configurations',
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
    );
    $form['configuration']['site_frontpage'] = array(
        '#title' => 'Configurations template of front page',
        '#type' => 'radios',
        '#options' => array (
         'front-page-one'   => t('First template'),
         'front-page-two'   => t('Second template'),
         'front-page-three' => t('Third  template'),
        ),
        '#required' => TRUE,
        '#default_value' => variable_get('site_frontpage'),
        '#description' => '',
    );
    return system_settings_form($form);
}
