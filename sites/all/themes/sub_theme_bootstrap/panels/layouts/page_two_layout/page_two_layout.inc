<?php
$plugin = array(
    'title' => t('Page two layout'),
    'category' => t('Page two layout'),
    'icon'  => 'page_two_layout.png',
    'theme' => 'page_two_layout',
    'css'   => 'page_two_layout.css',
    'regions' => array(
        'col1' => t('column one'),
     ),
);
function page_two_layout_ctools_plugin_directory($module, $plugin) {
    if (in_array($module, array('panelizer', 'ctools', 'page_manager', 'panels'))) {
        return 'plugins/' . $plugin;
    }
}