<?php
$plugin = array(
    'title' => t('Page one layout'),
    'category' => t('Page one layout'),
    'icon'  => 'page_one_layout.png',
    'theme' => 'page_one_layout',
    'css'   => 'page_one_layout.css',
    'regions' => array(
        'col1' => t('column one'),
        'col2' => t('column two'),
     ),
);
function page_one_layout_ctools_plugin_directory($module, $plugin) {
    if (in_array($module, array('panelizer', 'ctools', 'page_manager', 'panels'))) {
        return 'plugins/' . $plugin;
    }
}
