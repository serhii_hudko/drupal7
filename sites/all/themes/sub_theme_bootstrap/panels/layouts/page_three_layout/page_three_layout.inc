<?php
$plugin = array(
    'title' => t('Page three layout'),
    'category' => t('Page three layout'),
    'icon'  => 'page_three_layout.png',
    'theme' => 'page_three_layout',
    'css'   => 'page_three_layout.css',
    'regions' => array(
        'col1' => t('column one'),
        'col2' => t('column two'),
     ),
);
function page_three_layout_ctools_plugin_directory($module, $plugin) {
    if (in_array($module, array('panelizer', 'ctools', 'page_manager', 'panels'))) {
        return 'plugins/' . $plugin;
    }
}