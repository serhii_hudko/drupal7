<?php
/**
 * @file
 * Template for Site News layout.
 */
?>

<div class="panel-display panel-4col-stacked clearfix" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
    <div class="center-wrapper clearfix">
        <div class="panel-panel panel-col panel-col1 panel-col-first">
            <div class="col-md-12"><div class="inside"><?php print $content['col1']; ?></div></div>
            <div class="col-md-12"><div class="inside"><?php print $content['col2']; ?></div></div>
        </div>
    </div>
</div>
